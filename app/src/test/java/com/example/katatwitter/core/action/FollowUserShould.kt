package com.example.katatwitter.core.action

import com.example.katatwitter.core.repository.UserService
import io.mockk.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test

@ExperimentalCoroutinesApi
class FollowUserShould {
    private lateinit var userService: UserService
    private lateinit var followUser: FollowUser
    private lateinit var actionData: FollowUser.ActionData

    @Before
    fun setUp() {
        userService = mockk(relaxUnitFun = true)
        followUser = FollowUser(userService)
    }

    @Test
    fun `user can follow another user`() = runTest {
        givenAnUserFollowingAnotherUser()
        whenFollowUser()
        thenVerifyMethodIsCalled()
    }


    private fun givenAnUserFollowingAnotherUser() {
        actionData = FollowUser.ActionData(FOLLOWER, FOLLOWED)
    }

    private suspend fun whenFollowUser() {
        followUser(actionData)
    }

    private fun thenVerifyMethodIsCalled() {
        coVerify { userService.follow(FOLLOWER, FOLLOWED) }
    }

    private companion object {
        const val FOLLOWER = "@jp"
        const val FOLLOWED = "@amc"
    }
}