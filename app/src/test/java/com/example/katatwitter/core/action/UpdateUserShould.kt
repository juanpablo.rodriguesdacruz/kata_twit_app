package com.example.katatwitter.core.action

import com.example.katatwitter.core.domain.User
import com.example.katatwitter.core.domain.exception.UserDoesNotExistException
import com.example.katatwitter.core.repository.UserService
import io.mockk.*
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test

class UpdateUserShould {
    private lateinit var userService: UserService
    private lateinit var updateUser: UpdateUser
    private lateinit var actionData: UpdateUser.ActionData
    private var error: Throwable? = null

    @Before
    fun setUp() {
        userService = mockk(relaxUnitFun = true)
        updateUser=  UpdateUser(userService)
    }

    @Test
    fun `user can update his real name`() = runTest {
        givenAnUser()
        givenAnExistingUser()
        whenUpdateUser()
        thenUserWasUpdated()
    }

    @Test
    fun `not update real name when user does not exists`() = runTest {
        givenAnUser()
        givenANullUser()

        error = runCatching {
            whenUpdateUser()
        }.exceptionOrNull()

        thenUserWasNotUpdated()
    }

    @Test(expected = UserDoesNotExistException::class)
    fun `throw exception when nickName is not exists`() = runTest {
        givenAnUser()
        givenANullUser()
        whenUpdateUser()
    }

    private fun givenAnUser() {
        actionData = UpdateUser.ActionData(REAL_NAME, NICK_NAME)
    }

    private fun givenAnExistingUser() {
        coEvery { userService.find(actionData.nickName) } returns USER
    }

    private fun givenANullUser() {
        coEvery { userService.find(actionData.nickName) } returns null
    }

    private suspend fun whenUpdateUser() {
        updateUser(actionData)
    }

    private fun thenUserWasUpdated() {
        coVerify { userService.update(USER) }
    }

    private fun thenUserWasNotUpdated() {
        coVerify(exactly = 0) { userService.update(any()) }
    }

    private companion object {
        const val REAL_NAME = "Eddie Van Halen"
        const val NICK_NAME = "@evh"
        val USER = User(REAL_NAME, NICK_NAME)
    }
}
