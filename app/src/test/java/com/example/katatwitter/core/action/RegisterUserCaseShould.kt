package com.example.katatwitter.core.action

import com.example.katatwitter.core.domain.User
import com.example.katatwitter.core.domain.exception.UserExistsException
import com.example.katatwitter.core.repository.UserService
import io.mockk.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test

@ExperimentalCoroutinesApi
class RegisterUserCaseShould {
    private lateinit var userService: UserService
    private lateinit var registerUser: RegisterUser
    private lateinit var actionData: RegisterUser.ActionData

    @Before
    fun setUp() {
        userService = mockk(relaxUnitFun = true)
        registerUser = RegisterUser(userService)
    }

    @Test
    fun `register user`() = runTest {
        givenAnUser()
        givenANullUser()

        whenRegister()

        thenUserWasRegistered()
    }

    @Test
    fun `do not register player when nickname is not available`() = runTest{
        givenAnUser()
        givenAnExistingUser()
        try {
            whenRegister()
        } catch (e: Exception) {
            thenUserWasNotRegistered()
        }
    }

    @Test(expected = UserExistsException::class)
    fun `throw exception when nickname is not available`() = runTest {
        givenAnUser()
        givenAnExistingUser()
        whenRegister()
    }

    private fun givenAnUser() {
        actionData = RegisterUser.ActionData(REAL_NAME, NICK_NAME)
    }

    private fun givenANullUser() {
        coEvery { userService.find(NICK_NAME) } returns null
    }

    private fun givenAnExistingUser() {
        coEvery { userService.find(NICK_NAME) } returns USER
    }

    private suspend fun whenRegister() {
        registerUser(actionData)
    }

    private fun thenUserWasRegistered() {
        coVerify { userService.save(USER) }
    }

    private fun thenUserWasNotRegistered() {
        coVerify(exactly = 0) { userService.save(any()) }
    }

    private companion object {
        const val REAL_NAME = "Eddie Van Halen"
        const val NICK_NAME = "@evh"
        val USER = User(REAL_NAME, NICK_NAME)
    }
}