package com.example.katatwitter.core.action

import com.example.katatwitter.core.repository.UserService
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test

@ExperimentalCoroutinesApi
class CreateTwitShould {
    private lateinit var repository: UserService
    private lateinit var createTwit: CreateTwit
    private lateinit var actionData: CreateTwit.ActionData

    @Before
    fun setUp() {
        repository = mockk(relaxUnitFun = true)
        createTwit = CreateTwit(repository)
    }

    @Test
    fun `create twit`() = runTest {
        givenATwit()
        whenCreateATwit()
        thenVerifyMethodIsCalled()
    }

    private fun givenATwit() {
        actionData = CreateTwit.ActionData(NICK_NAME, TWIT)
    }

    private suspend fun whenCreateATwit() {
        createTwit(actionData)
    }

    private fun thenVerifyMethodIsCalled() {
        coVerify { repository.twit(NICK_NAME, TWIT) }
    }


    private companion object {
        const val NICK_NAME = "@jackBauer"
        const val TWIT = "twit test"
    }
}
