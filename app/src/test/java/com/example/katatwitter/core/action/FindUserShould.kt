package com.example.katatwitter.core.action

import com.example.katatwitter.core.domain.User
import com.example.katatwitter.core.domain.exception.UserDoesNotExistException
import com.example.katatwitter.core.repository.UserService
import io.mockk.coEvery
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.test.runTest
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

class FindUserShould {
    private lateinit var findUser: FindUser
    private lateinit var userService: UserService
    private lateinit var actionData: FindUser.ActionData
    private lateinit var user: User

    @Before
    fun setUp() {
        userService = mockk()
        findUser = FindUser(userService)
        actionData = FindUser.ActionData(NICK_NAME)
    }

    @Test
    fun `find an user`() = runTest {
        givenAnUser()
        whenFindAnUser()
        thenReturnAnExistingUser()
    }

    @Test(expected = UserDoesNotExistException::class)
    fun `cant find an user`() = runTest {
        givenANullUser()
        whenFindAnUser()
    }

    private fun givenAnUser() {
        coEvery { userService.find(NICK_NAME) } returns USER
    }

    private fun givenANullUser() {
        coEvery { userService.find(NICK_NAME) } returns null
    }

    private suspend fun whenFindAnUser() {
        user = findUser(actionData)
    }

    private fun thenReturnAnExistingUser() {
        assertEquals(NICK_NAME, user.nickName)
    }

    private companion object {
        const val REAL_NAME = "Jack Bauer"
        const val NICK_NAME = "@jackBauer"
        val USER = User(REAL_NAME, NICK_NAME)
    }
}