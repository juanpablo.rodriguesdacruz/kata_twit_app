package com.example.katatwitter.presentation.find

import com.example.katatwitter.CoroutineScopeRule
import com.example.katatwitter.core.action.FindUser
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class FindViewModelShould {
    @get:Rule
    val coroutineScope = CoroutineScopeRule()

    private lateinit var findUser: FindUser
    private lateinit var viewModel: FindViewModel
    private lateinit var actionData: FindUser.ActionData

    @Before
    fun setUp() {
        findUser = mockk(relaxUnitFun = true)
        viewModel = FindViewModel(findUser)
    }

    @Test
    fun `find user`() = runTest {
        givenAnUser()
        whenViewModelIsLaunched()
        thenVerifyMethodIsCalled()
    }

    private fun givenAnUser() {
        actionData = FindUser.ActionData(NICK_NAME)
    }

    private fun whenViewModelIsLaunched() {
        viewModel.onFindPress(NICK_NAME)
    }

    private fun thenVerifyMethodIsCalled() {
        coVerify { findUser.invoke(actionData) }
    }

    private companion object {
        const val NICK_NAME = "@evh"
    }
}