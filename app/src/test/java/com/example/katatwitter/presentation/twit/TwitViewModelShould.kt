package com.example.katatwitter.presentation.twit

import com.example.katatwitter.CoroutineScopeRule
import com.example.katatwitter.core.action.CreateTwit
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class TwitViewModelShould {
    @get:Rule
    val coroutineScope = CoroutineScopeRule()

    private lateinit var createTwit: CreateTwit
    private lateinit var viewModel: TwitViewModel
    private lateinit var actionData: CreateTwit.ActionData

    @Before
    fun setUp() {
        createTwit = mockk(relaxUnitFun = true)
        viewModel = TwitViewModel(createTwit)
    }

    @Test
    fun `create twit`() = runTest {
        givenAnUser()
        whenViewModelIsLaunched()
        thenVerifyMethodIsCalled()
    }

    private fun givenAnUser() {
        actionData = CreateTwit.ActionData(NICK_NAME, TWIT)
    }

    private fun whenViewModelIsLaunched() {
        viewModel.onTwitPress(NICK_NAME, TWIT)
    }

    private fun thenVerifyMethodIsCalled() {
        coVerify { createTwit.invoke(actionData) }
    }

    private companion object {
        const val NICK_NAME = "@evh"
        const val TWIT = "twit test"
    }
}
