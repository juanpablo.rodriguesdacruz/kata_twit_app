package com.example.katatwitter.presentation.update

import com.example.katatwitter.CoroutineScopeRule
import com.example.katatwitter.core.action.UpdateUser
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class UpdateViewModelShould {
    @get:Rule
    val coroutineScope = CoroutineScopeRule()

    private lateinit var updateUser: UpdateUser
    private lateinit var viewModel: UpdateViewModel
    private lateinit var actionData: UpdateUser.ActionData

    @Before
    fun setUp() {
        updateUser = mockk(relaxUnitFun = true)
        viewModel = UpdateViewModel(updateUser)
    }

    @Test
    fun `update user`() = runTest {
        givenAnUser()
        whenViewModelIsLaunched()
        thenVerifyMethodIsCalled()
    }

    private fun givenAnUser() {
        actionData = UpdateUser.ActionData(REAL_NAME, NICK_NAME)
    }

    private fun whenViewModelIsLaunched() {
        viewModel.onUpdatePress(REAL_NAME, NICK_NAME)
    }

    private fun thenVerifyMethodIsCalled() {
        coVerify { updateUser.invoke(actionData) }
    }

    private companion object {
        const val REAL_NAME = "Eddie Van Halen"
        const val NICK_NAME = "@evh"
    }
}