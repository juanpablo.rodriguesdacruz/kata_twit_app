package com.example.katatwitter.presentation.register

import com.example.katatwitter.CoroutineScopeRule
import com.example.katatwitter.core.action.RegisterUser
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class RegisterViewModelShould {
    @get:Rule
    val coroutineScope = CoroutineScopeRule()

    private lateinit var registerUser: RegisterUser
    private lateinit var viewModel: RegisterViewModel
    private lateinit var actionData: RegisterUser.ActionData

    @Before
    fun setUp() {
        registerUser = mockk(relaxUnitFun = true)
        viewModel = RegisterViewModel(registerUser)
    }

    @Test
    fun `register user`() = runTest {
        givenAnUser()
        whenViewModelIsLaunched()
        thenVerifyMethodIsCalled()
    }

    private fun givenAnUser() {
        actionData = RegisterUser.ActionData(REAL_NAME, NICK_NAME)
    }

    private fun whenViewModelIsLaunched() {
        viewModel.onCreatePress(REAL_NAME, NICK_NAME)
    }

    private fun thenVerifyMethodIsCalled() {
        coVerify { registerUser.invoke(actionData) }
    }

    private companion object {
        const val REAL_NAME = "Eddie Van Halen"
        const val NICK_NAME = "@evh"
    }
}