package com.example.katatwitter.presentation.follow

import com.example.katatwitter.CoroutineScopeRule
import com.example.katatwitter.core.action.FollowUser
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class FollowViewModelShould {
    @get:Rule
    val coroutineScope = CoroutineScopeRule()

    private lateinit var followUser: FollowUser
    private lateinit var viewModel: FollowViewModel
    private lateinit var actionData: FollowUser.ActionData

    @Before
    fun setUp() {
        followUser = mockk(relaxUnitFun = true)
        viewModel = FollowViewModel(followUser)
    }

    @Test
    fun `follow user`() = runTest {
        givenAFollowerAndFollowed()
        whenViewModelIsLaunched()
        thenVerifyMethodIsCalled()
    }

    private fun givenAFollowerAndFollowed() {
        actionData = FollowUser.ActionData(FOLLOWER, FOLLOWED)
    }

    private fun whenViewModelIsLaunched() {
        viewModel.onFollowPress(FOLLOWER, FOLLOWED)
    }

    private fun thenVerifyMethodIsCalled() {
        coVerify { followUser.invoke(actionData) }
    }

    private companion object {
        const val FOLLOWER = "@jp"
        const val FOLLOWED = "@amc"
    }
}
