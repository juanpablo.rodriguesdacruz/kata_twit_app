package com.example.katatwitter.infrastructure

import com.example.katatwitter.core.action.*
import com.example.katatwitter.infrastructure.conexion.RetrofitFactory
import com.example.katatwitter.infrastructure.conexion.UserApiClient
import com.example.katatwitter.infrastructure.service.ApiUserService

object Factory {
    private val service by lazy { ApiUserService(getApiUserService()) }
    fun createRegisterUser(): RegisterUser {
        return RegisterUser(service)
    }

    fun updateUser(): UpdateUser {
        return UpdateUser(service)
    }

    fun followUser(): FollowUser {
        return FollowUser(service)
    }

    fun findUser(): FindUser {
        return FindUser(service)
    }

    fun createTwit(): CreateTwit {
        return CreateTwit(service)
    }

    private fun getApiUserService(): UserApiClient {
        return RetrofitFactory.retrofit.create(UserApiClient::class.java)
    }
}
