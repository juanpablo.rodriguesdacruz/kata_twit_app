package com.example.katatwitter.infrastructure.conexion


import com.example.katatwitter.core.domain.User
import com.google.gson.annotations.SerializedName
import retrofit2.Response
import retrofit2.http.*

interface UserApiClient {
    @PUT("user/{nickName}/{realName}")
    suspend fun registerUser(
        @Path("nickName") nickName: String,
        @Path("realName") realName: String
    )

    @GET("user/{nickName}/find")
    suspend fun findUser(
        @Path("nickName") nickName: String,
    ): UserRepresentation?

    @POST("user/{follower}/follow/{followed}")
    suspend fun followUser(
        @Path("follower") nickName: String,
        @Path("followed") realName: String
    )

    @POST("user/{nickName}/{realName}")
    suspend fun updateUser(
        @Path("nickName") nickName: String,
        @Path("realName") realName: String
    )

    @POST("user/{nickName}/twit")
    suspend fun createTwit(
        @Path("nickName") nickName: String,
        @Body twit: TwitRepresentation
    )
}

data class UserRepresentation(
    val nickName: String,
    val realName: String,
    val followers: MutableList<FollowerRepresentation>,
    val twits: MutableList<TwitRepresentation>
)

data class TwitRepresentation(
    @SerializedName("twit") val twit: String
)

data class FollowerRepresentation(
    @SerializedName("follower") val follower: String
)
