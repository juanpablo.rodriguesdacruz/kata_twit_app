package com.example.katatwitter.infrastructure.service

import com.example.katatwitter.core.domain.User
import com.example.katatwitter.core.domain.toUser
import com.example.katatwitter.core.repository.UserService
import com.example.katatwitter.infrastructure.conexion.TwitRepresentation
import com.example.katatwitter.infrastructure.conexion.UserApiClient
import com.example.katatwitter.infrastructure.conexion.UserRepresentation

class ApiUserService(private val userApiClient: UserApiClient): UserService {
    override suspend fun save(user: User) {
        userApiClient.registerUser(user.nickName, user.realName)
    }

    override suspend fun update(user: User) {
        userApiClient.updateUser(user.nickName, user.realName)
    }

    override suspend fun find(nickname: String): User? {
        return userApiClient.findUser(nickname)?.run {
            this.toUser()
        }
    }

    override suspend fun twit(nickname: String, twit: String) {
        userApiClient.createTwit(nickname, TwitRepresentation(twit))
    }

    override suspend fun follow(follower: String, followed: String) {
        userApiClient.followUser(follower, followed)
    }
}
