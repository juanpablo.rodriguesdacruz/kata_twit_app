package com.example.katatwitter.core.action

import com.example.katatwitter.core.repository.UserService

class CreateTwit (private val repository: UserService) {
    suspend operator fun invoke(actionData: ActionData) {
        repository.twit(actionData.nickName, actionData.twit)
    }

    data class ActionData(
        val nickName : String,
        val twit: String
    )
}