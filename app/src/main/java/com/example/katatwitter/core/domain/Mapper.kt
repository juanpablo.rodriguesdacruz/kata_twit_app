package com.example.katatwitter.core.domain

import com.example.katatwitter.infrastructure.conexion.UserRepresentation

fun UserRepresentation.toUser() = User(
    realName,
    nickName,
    this.followers.map { it.follower } as MutableList,
    this.twits.map { it.twit } as MutableList
)
