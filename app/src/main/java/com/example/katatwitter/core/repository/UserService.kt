package com.example.katatwitter.core.repository

import com.example.katatwitter.core.domain.User

interface UserService {
    suspend fun save(user: User)
    suspend fun update(user: User)
    suspend fun find(nickname: String): User?
    suspend fun twit(nickname: String, twit: String)
    suspend fun follow(follower: String, followed: String)
}
