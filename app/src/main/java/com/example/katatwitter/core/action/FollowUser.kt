package com.example.katatwitter.core.action

import com.example.katatwitter.core.repository.UserService

class FollowUser(private val userService: UserService) {
    suspend operator fun invoke(actionData: ActionData) {
       userService.follow(actionData.follower, actionData.followed)
    }

    data class ActionData(
        val follower: String,
        val followed: String)
}
