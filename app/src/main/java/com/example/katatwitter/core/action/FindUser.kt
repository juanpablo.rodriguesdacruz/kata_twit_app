package com.example.katatwitter.core.action

import com.example.katatwitter.core.domain.User
import com.example.katatwitter.core.repository.UserService

class FindUser(private val repository: UserService) {
    suspend operator fun invoke(actionData: ActionData): User? {
        return repository.find(actionData.nickName)
    }

    data class ActionData(
        val nickName : String
    )
}
