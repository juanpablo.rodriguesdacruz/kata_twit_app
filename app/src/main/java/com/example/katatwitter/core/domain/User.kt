package com.example.katatwitter.core.domain

data class User(
    val realName: String,
    val nickName: String,
    val followers: MutableList<String>?,
    val twit: MutableList<String>?
)
