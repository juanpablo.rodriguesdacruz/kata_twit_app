package com.example.katatwitter.core.action

import com.example.katatwitter.core.domain.User
import com.example.katatwitter.core.repository.UserService

class RegisterUser(private val userService: UserService) {
    suspend operator fun invoke(actionData: ActionData) {
        val user = User(actionData.realName, actionData.nickName, null, null)
        userService.save(user)
    }

    data class ActionData(
        val realName : String,
        val nickName : String
    )
}