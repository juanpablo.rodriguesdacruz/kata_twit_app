package com.example.katatwitter.presentation.twit

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.katatwitter.core.action.CreateTwit
import kotlinx.coroutines.launch

class TwitViewModel (private val createTwit: CreateTwit): ViewModel() {
    fun onTwitPress(nickName: String, twit: String) {
        viewModelScope.launch {
            val action = CreateTwit.ActionData(nickName, twit)
            createTwit(action)
        }
    }
}
