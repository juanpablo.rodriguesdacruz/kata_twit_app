package com.example.katatwitter.presentation.register

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.katatwitter.core.action.RegisterUser
import kotlinx.coroutines.launch

class RegisterViewModel(private val registerUser: RegisterUser): ViewModel() {
    fun onCreatePress(realName: String, nickName: String) {
        viewModelScope.launch {
            val actionData = RegisterUser.ActionData(realName,nickName)
            registerUser(actionData)
        }
    }
}
