package com.example.katatwitter.presentation.register

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import androidx.fragment.app.viewModels
import com.example.katatwitter.R
import com.example.katatwitter.databinding.FragmentRegisterBinding

class RegisterFragment : Fragment(R.layout.fragment_register) {
    private  val registerViewModel by viewModels<RegisterViewModel> { RegisterViewModelFactory() }
    lateinit var binding: FragmentRegisterBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentRegisterBinding.bind(view)

        binding.buttonRegister.setOnClickListener {
            val realName = binding.editTextRealName.text.toString()
            val nickName = binding.editTextNickName.text.toString()
            registerViewModel.onCreatePress(realName, nickName)
        }
    }
}