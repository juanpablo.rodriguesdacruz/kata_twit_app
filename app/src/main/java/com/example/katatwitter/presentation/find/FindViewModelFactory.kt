package com.example.katatwitter.presentation.find

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.katatwitter.infrastructure.Factory

class FindViewModelFactory: ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return FindViewModel(Factory.findUser()) as T
    }
}
