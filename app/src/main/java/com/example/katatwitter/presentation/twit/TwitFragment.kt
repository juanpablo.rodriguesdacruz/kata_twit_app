package com.example.katatwitter.presentation.twit

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.example.katatwitter.R
import com.example.katatwitter.databinding.FragmentTwitBinding

class TwitFragment : Fragment(R.layout.fragment_twit) {
    private  val twitViewModel by viewModels<TwitViewModel> { TwitViewModelFactory() }
    lateinit var binding: FragmentTwitBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentTwitBinding.bind(view)
        binding.buttonTwit.setOnClickListener {
            val nickname = binding.editTextUser.text.toString()
            val twit = binding.editTextTwit.text.toString()
            twitViewModel.onTwitPress(nickname, twit)
        }
    }
}
