package com.example.katatwitter.presentation.follow

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.katatwitter.infrastructure.Factory

class FollowViewModelFactory: ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return FollowViewModel(Factory.followUser()) as T
    }
}