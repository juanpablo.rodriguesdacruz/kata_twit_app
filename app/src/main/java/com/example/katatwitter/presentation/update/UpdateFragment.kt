package com.example.katatwitter.presentation.update

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import androidx.fragment.app.viewModels
import com.example.katatwitter.R
import com.example.katatwitter.databinding.FragmentUpdateBinding

class UpdateFragment : Fragment(R.layout.fragment_update) {
    private  val updateViewModel by viewModels<UpdateViewModel> { UpdateViewModelFactory() }
    private lateinit var binding: FragmentUpdateBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentUpdateBinding.bind(view)

        binding.buttonUpdate.setOnClickListener {
            val realName = binding.editTextRealNameUp.text.toString()
            val nickName = binding.editTextNickNameUp.text.toString()
            updateViewModel.onUpdatePress(realName, nickName)
        }
    }
}