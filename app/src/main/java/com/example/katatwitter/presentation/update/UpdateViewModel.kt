package com.example.katatwitter.presentation.update

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.katatwitter.core.action.UpdateUser
import kotlinx.coroutines.launch

class UpdateViewModel(private val updateUser: UpdateUser): ViewModel(){
    fun onUpdatePress(realName: String, nickName: String) {
        viewModelScope.launch {
            val action = UpdateUser.ActionData(realName, nickName)
            updateUser(action)
        }
    }
}
