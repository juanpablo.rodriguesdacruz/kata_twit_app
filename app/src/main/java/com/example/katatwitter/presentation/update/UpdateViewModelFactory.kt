package com.example.katatwitter.presentation.update

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.katatwitter.infrastructure.Factory

class UpdateViewModelFactory : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return UpdateViewModel(Factory.updateUser()) as T
    }
}