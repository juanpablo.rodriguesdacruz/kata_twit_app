package com.example.katatwitter.presentation.follow

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import androidx.fragment.app.viewModels
import com.example.katatwitter.R
import com.example.katatwitter.databinding.FragmentFollowBinding

class FollowFragment : Fragment(R.layout.fragment_follow) {
    private  val followViewModel by viewModels<FollowViewModel> { FollowViewModelFactory() }
    private lateinit var binding: FragmentFollowBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentFollowBinding.bind(view)
        binding.buttonFollow.setOnClickListener {
            val follower = binding.editTextFollower.text.toString()
            val followed = binding.editTextFollowed.text.toString()
            followViewModel.onFollowPress(follower, followed)
        }
    }
}