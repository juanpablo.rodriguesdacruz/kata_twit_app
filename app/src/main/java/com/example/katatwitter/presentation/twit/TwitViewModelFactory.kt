package com.example.katatwitter.presentation.twit

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.katatwitter.infrastructure.Factory

class TwitViewModelFactory: ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return TwitViewModel(Factory.createTwit()) as T
    }
}