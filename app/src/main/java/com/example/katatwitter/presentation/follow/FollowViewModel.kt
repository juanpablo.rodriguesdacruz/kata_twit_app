package com.example.katatwitter.presentation.follow

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.katatwitter.core.action.FollowUser
import kotlinx.coroutines.launch

class FollowViewModel(private val followUser: FollowUser): ViewModel() {
    fun onFollowPress(follower: String, followed: String) {
        viewModelScope.launch {
            val action = FollowUser.ActionData(follower,followed)
            followUser(action)
        }
    }
}
