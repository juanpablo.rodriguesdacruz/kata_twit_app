package com.example.katatwitter.presentation.find

import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.example.katatwitter.R
import com.example.katatwitter.databinding.FragmentFindBinding
import com.example.katatwitter.infrastructure.conexion.FollowerRepresentation
import com.example.katatwitter.infrastructure.conexion.TwitRepresentation

class FindFragment : Fragment(R.layout.fragment_find) {
    private  val findViewModel by viewModels<FindViewModel> { FindViewModelFactory() }
    private lateinit var binding: FragmentFindBinding
    private lateinit var adapterTwit: ArrayAdapter<*>
    private lateinit var adapterFollowers: ArrayAdapter<*>
    private var listTwit = mutableListOf<String>()
    private var listFollowers = mutableListOf<String>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentFindBinding.bind(view)
        binding.buttonFind.setOnClickListener {
            val nickName = binding.editTextFind.text.toString()
            findViewModel.onFindPress(nickName)
        }

        findViewModel.user.observe(viewLifecycleOwner) {user ->
            binding.textViewRealName.text = user?.realName ?: ""
            binding.textViewNickName.text = user?.nickName ?: ""
            listTwit = user?.twit!!
            listFollowers = user.followers!!
            adapterFollowers = ArrayAdapter(requireActivity(), android.R.layout.simple_list_item_1, listFollowers)
            binding.listviewFollowed.adapter = adapterFollowers
            adapterTwit = ArrayAdapter(requireActivity(), android.R.layout.simple_list_item_1, listTwit)
            binding.listviewTwit.adapter = adapterTwit

        }
    }
}