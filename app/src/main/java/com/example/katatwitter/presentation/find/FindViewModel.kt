package com.example.katatwitter.presentation.find

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.katatwitter.core.action.FindUser
import com.example.katatwitter.core.domain.User
import kotlinx.coroutines.launch

class FindViewModel (private val findUser: FindUser): ViewModel() {

    private var _user = MutableLiveData<User?>()
    val user : LiveData<User?>
        get() = _user
    fun onFindPress(nickName: String) {
        viewModelScope.launch {
            val action = FindUser.ActionData(nickName)
            _user.postValue(findUser(action))
        }
    }
}
