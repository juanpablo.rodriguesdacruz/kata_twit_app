package com.example.katatwitter.presentation.register

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.katatwitter.infrastructure.Factory

class RegisterViewModelFactory: ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return RegisterViewModel(Factory.createRegisterUser()) as T
    }
}
